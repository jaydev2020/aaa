import { Ifo } from './types'

const ifos: Ifo[] = [
  {
    id: 'IFO',
    address: '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3',
    isActive: true,
    name: 'Bread Swap',
    subTitle: 'Bread Swap - The Best Modern Yield Farm on Binance Smart Chain',
    description:
      'Bread Swap - The Best Modern Yield Farm on Binance Smart Chain.',
    launchDate: 'February. 18',
    launchTime: '8AM UTC',
    saleAmount: '1,000,000 BREAD',
    raiseAmount: '$000,000',
    pizzaToBurn: '$000,000',
    projectSiteUrl: 'https://breadswap.finance/',
    currency: 'BREAD-BNB LP',
    currencyAddress: '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3',
    tokenDecimals: 18,
    releaseBlockNumber: 28091254,
  },
]

export default ifos
